#include "Jeu.h"
#include "Martien.h"
#include "UIkit.h"
#include <fstream>

Jeu::Jeu() {  //:lesmartiens(2000, 2000, 20) 
	// ATTENTION: maxNbMartien: a partir de 10
	// ATTENTION: delayMouvement: a partir de 250 (soit la vitesse du mouvement de bouche) (MonMartien.cpp)
	// PARAMETERES: (delayMouvement,delayTirer,maxNbMartien);
}

Jeu::~Jeu(){
}

void Jeu::afficher_menu(Menu &leMenu) {
	leMenu.loadingScreen();
	leMenu.addOption("Commmencer");
	leMenu.addOption("Difficulte");
	leMenu.addOption("Scores");
	leMenu.addOption("Options avancee");
	leMenu.addOption("Exit");

	while (leMenu.navigationMenu() != ENTER || leMenu.getSelectedOption() != 0)
		leMenu.navigationMenu();


	system("cls");
	leMenu.afficherItem("3", 75, 15);
	leMenu.attendre(1000);
	system("cls");
	leMenu.afficherItem("2", 75, 15);
	leMenu.attendre(1000);
	system("cls");
	leMenu.afficherItem("1", 75, 15);
	leMenu.attendre(1000);
	system("cls");
	leMenu.afficherItem("go !", 75, 15);
	leMenu.attendre(700);
	system("cls");
	UIKit::cadre(40, 0, 180, 57, 70);
}

void Jeu::former_martiens(LesMartiens &LesMartiens) {
	LesMartiens.superviser_retirer_Laser();
	LesMartiens.jiggleLesMartiens();
	LesMartiens.creerNouveauxMartiens();
	LesMartiens.tirer();
	LesMartiens.deplacerLaser();
}

void Jeu::former_vaisseau_et_tirer_laser(MonVaisseau &unVaisseau){
	unVaisseau.actionsVaisseau();
	unVaisseau.deplacerLasers();
}













//////////////////////           REFAIRE METHODE    (DEBUT)             //////////////////////////////////////////

//// si laser de martien arrive au fond, le faire disparaitre
//// si laser de martien rencontrer vaisseau, fin du jeu
//void Jeu::superviserLaserMartien(LesMartiens & lesmartiens, MonVaisseau &unVaisseau){
//	// parcourir tableau de lasers type 0
//	for (int i = 0; i < lesmartiens.nbLaserType0; i++) {
//		int posX = lesmartiens.tbLasersType0[i].getPosX();
//		int posY = lesmartiens.tbLasersType0[i].getPosY();
//
//		// si atteindre la ligne plus bas
//		if (posY == 50){
//			//enlever sa place dans tableau
//			lesmartiens.tbLasersType0[i] = lesmartiens.tbLasersType0[lesmartiens.nbLaserType0 - 1];
//			//effacer la derniere position
//			UIKit::gotoXY(posX, posY);
//			cout << "  ";
//
//			lesmartiens.nbLaserType0--;
//		}
//
//		//si toucher le vaisseau
//		int vaisseau_x = unVaisseau.coord.getPositionX();
//		int vaisseau_y = unVaisseau.coord.getPositionY();
//		if (posY == vaisseau_y && posX >= vaisseau_x - 1 && posX <= vaisseau_x + 3){
//			UIKit::gotoXY(vaisseau_x, vaisseau_y);
//			cout << "_\\^/_";
//			UIKit::gotoXY(vaisseau_x, vaisseau_y + 1);
//			cout << ">_ _<";
//			UIKit::gotoXY(vaisseau_x, vaisseau_y + 1);
//			cout << "/^\\";
//			gameOver_VaisseauDetruit = true;
//		}
//	}
//
//	// parcourir tableau de lasers type 1
//	for (int i = 0; i < lesmartiens.nbLaserType1; i++) {
//		int posX = lesmartiens.tbLasersType1[i].getPosX();
//		int posY = lesmartiens.tbLasersType1[i].getPosY();
//		// si atteindre la ligne plus bas
//		if (posY == 50){
//			//enlever sa place dans tableau
//			lesmartiens.tbLasersType1[i] = lesmartiens.tbLasersType1[lesmartiens.nbLaserType1 - 1];
//			//effacer la derniere position
//			UIKit::gotoXY(posX, posY);
//			cout << "  ";
//
//			lesmartiens.nbLaserType1--;
//		}
//
//		//si toucher le vaisseau
//		int vaisseau_x = unVaisseau.coord.getPositionX();
//		int vaisseau_y = unVaisseau.coord.getPositionY();
//		if (posY == vaisseau_y && posX >= vaisseau_x - 1 && posX <= vaisseau_x + 3){
//			UIKit::gotoXY(vaisseau_x, vaisseau_y);
//			cout << "_\\^/_";
//			UIKit::gotoXY(vaisseau_x, vaisseau_y + 1);
//			cout << ">_ _<";
//			UIKit::gotoXY(vaisseau_x, vaisseau_y + 1);
//			cout << "/^\\";
//			gameOver_VaisseauDetruit = true;
//		}
//	}
//}

// si laser de martien arrive au fond, le faire disparaitre
// si laser de martien rencontrer vaisseau, fin du jeu
void Jeu::superviserLaserMartien(LesMartiens & lesmartiens, MonVaisseau &unVaisseau){

	// parcourir tableau de lasers type 0
	_superviserLaserMartien(lesmartiens.tbLasersType0, lesmartiens.nbLaserType0, unVaisseau);

	// parcourir tableau de lasers type 1
	_superviserLaserMartien(lesmartiens.tbLasersType1, lesmartiens.nbLaserType1, unVaisseau);
}

void Jeu::_superviserLaserMartien(MonLaser* tbLasers, int nbLaser, MonVaisseau &unVaisseau)
{
	for (int i = 0; i < nbLaser; i++) {
		int posX = tbLasers[i].getPosX();
		int posY = tbLasers[i].getPosY();

		// si atteindre la ligne plus bas
		if (posY == 50){
			//enlever sa place dans tableau
			tbLasers[i] = tbLasers[nbLaser - 1];
			//effacer la derniere position
			UIKit::gotoXY(posX, posY);
			cout << "  ";

			nbLaser--;
		}

		//si toucher le vaisseau
		int vaisseau_x = unVaisseau.coord.getPositionX();
		int vaisseau_y = unVaisseau.coord.getPositionY();
		if (posY == vaisseau_y && posX >= vaisseau_x - 1 && posX <= vaisseau_x + 3){
			UIKit::gotoXY(vaisseau_x, vaisseau_y);
			cout << "_\\^/_";
			UIKit::gotoXY(vaisseau_x, vaisseau_y + 1);
			cout << ">_ _<";
			UIKit::gotoXY(vaisseau_x, vaisseau_y + 1);
			cout << "/^\\";
			gameOver_VaisseauDetruit = true;
		}
	}
}

//////////////////////           REFAIRE METHODE (FIN)                //////////////////////////////////////////











//////////////////////           REFAIRE METHODE    (DEBUT)             //////////////////////////////////////////

//// si laser de vaisseau arrive en haut, le faire disparaitre
//// si laser de vaisseau rencontrer un martien, retirer ce martien
//void Jeu::superviserLaserVaisseau(LesMartiens & lesmartiens, MonVaisseau &unVaisseau){
//	for (int i = 0; i < unVaisseau.nbLaser; i++){
//		int laserVaisseau_x = unVaisseau.lesLasers[i].coord.getPositionX();
//		int laserVaisseau_y = unVaisseau.lesLasers[i].coord.getPositionY();
//
//
//		// I. tester si le laser touche un martien du type 0
//		int j = 0;
//		bool fin_0 = false;
//		while (!fin_0 && j < lesmartiens.nbMartien_0){
//			int posX_martien = lesmartiens.tbMonMartiens_0[j]->coord.getPositionX();
//			int posY_martien = lesmartiens.tbMonMartiens_0[j]->coord.getPositionY();
//
//			if (laserVaisseau_y == posY_martien + 1
//				&& laserVaisseau_x >= posX_martien - 1 && laserVaisseau_x <= posX_martien + 3)
//				fin_0 = true;
//
//			j++;
//		}
//
//		if (fin_0){
//			// 1. retire le martien
//			lesmartiens.tbMonMartiens_0[j - 1]->removeExtraTerrestre();
//			delete lesmartiens.tbMonMartiens_0[j - 1];
//			lesmartiens.tbMonMartiens_0[j - 1] = lesmartiens.tbMonMartiens_0[lesmartiens.nbMartien_0 - 1];
//			//	lesmartiens.tbMonMartiens_0[j - 1]->isAlive = false;
//			lesmartiens.nbMartien_0--;
//			score += 300;
//			// 2. retirer le laser du vaisseau
//			unVaisseau.lesLasers[i] = unVaisseau.lesLasers[unVaisseau.nbLaser - 1];
//			UIKit::gotoXY(laserVaisseau_x, laserVaisseau_y);
//			cout << "  ";
//			unVaisseau.nbLaser--;
//		}
//
//
//
//
//		// II. tester si le laser touche un martien du type 1
//		int k = 0;
//		bool fin_1 = false;
//		while (!fin_1 && k < lesmartiens.nbMartien_1){
//			int posX_martien = lesmartiens.tbMonMartiens_1[k]->coord.getPositionX();
//			int posY_martien = lesmartiens.tbMonMartiens_1[k]->coord.getPositionY();
//
//			if (laserVaisseau_y == posY_martien + 1
//				&& laserVaisseau_x >= posX_martien - 1 && laserVaisseau_x <= posX_martien + 3)
//				fin_1 = true;
//
//			k++;
//		}
//
//		if (fin_1){
//			// 1. retire le martien
//			lesmartiens.tbMonMartiens_1[k - 1]->removeExtraTerrestre();
//			delete lesmartiens.tbMonMartiens_1[k - 1];
//			lesmartiens.tbMonMartiens_1[k - 1] = lesmartiens.tbMonMartiens_1[lesmartiens.nbMartien_1 - 1];
//			//	lesmartiens.tbMonMartiens_1[k - 1]->isAlive = false;
//			lesmartiens.nbMartien_1--;
//			score += 450;
//			// 2. retirer le laser du vaisseau
//			unVaisseau.lesLasers[i] = unVaisseau.lesLasers[unVaisseau.nbLaser - 1];
//			UIKit::gotoXY(laserVaisseau_x, laserVaisseau_y);
//			cout << "  ";
//			unVaisseau.nbLaser--;
//		}
//
//
//		char collision_laser = UIKit::getCharXY(laserVaisseau_x, laserVaisseau_y - 1);
//
//		// III. si le laser arrive en haut, le retirer du tableau
//		if (laserVaisseau_y == 3 || collision_laser == '<'){
//			unVaisseau.lesLasers[i] = unVaisseau.lesLasers[unVaisseau.nbLaser - 1];
//			UIKit::gotoXY(laserVaisseau_x, laserVaisseau_y);
//			cout << "  ";
//			unVaisseau.nbLaser--;
//		}
//
//		if (lesmartiens.nbMartien_0 == 0 && lesmartiens.nbMartien_1 == 0)
//			gameOver_PlusDeMartien = true;
//	}
//}

// si laser de vaisseau arrive en haut, le faire disparaitre
// si laser de vaisseau rencontrer un martien, retirer ce martien
void Jeu::superviserLaserVaisseau(LesMartiens & lesmartiens, MonVaisseau &unVaisseau){
	for (int i = 0; i < unVaisseau.nbLaser; i++){
		int laserVaisseau_x = unVaisseau.lesLasers[i].coord.getPositionX();
		int laserVaisseau_y = unVaisseau.lesLasers[i].coord.getPositionY();

		// I. tester si le laser touche un martien du type 0
		testerCollisionMartien(lesmartiens.tbMonMartiens_0, lesmartiens.nbMartien_0,
			unVaisseau, laserVaisseau_x, laserVaisseau_y, score, i);

		// II. tester si le laser touche un martien du type 1
		testerCollisionMartien(lesmartiens.tbMonMartiens_1, lesmartiens.nbMartien_1,
			unVaisseau, laserVaisseau_x, laserVaisseau_y, score, i);

		char collision_laser = UIKit::getCharXY(laserVaisseau_x, laserVaisseau_y - 1);

		// III. si le laser arrive en haut, le retirer du tableau
		if (laserVaisseau_y == 3 || collision_laser == '<'){
			unVaisseau.lesLasers[i] = unVaisseau.lesLasers[unVaisseau.nbLaser - 1];
			UIKit::gotoXY(laserVaisseau_x, laserVaisseau_y);
			cout << "  ";
			unVaisseau.nbLaser--;
		}

		if (lesmartiens.nbMartien_0 == 0 && lesmartiens.nbMartien_1 == 0)
			gameOver_PlusDeMartien = true;
	}
}

void Jeu::testerCollisionMartien(MonMartien** tbMonMartiens, int &nbMartien, MonVaisseau &unVaisseau,
	int &laserVaisseau_x, int &laserVaisseau_y, int &score, int &i)
{
	int j = 0;
	bool fin = false;
	while (!fin && j < nbMartien){
		int posX_martien = tbMonMartiens[j] ->coord.getPositionX();
		int posY_martien = tbMonMartiens[j]->coord.getPositionY();

		if (laserVaisseau_y == posY_martien + 1
			&& laserVaisseau_x >= posX_martien - 1 && laserVaisseau_x <= posX_martien + 3)
			fin = true;

		j++;
	}

	if (fin){
		// 1. retire le martien
		tbMonMartiens[j - 1]->removeExtraTerrestre();
		delete tbMonMartiens[j - 1];
		tbMonMartiens[j - 1] = tbMonMartiens[nbMartien - 1];
		//	lesmartiens.tbMonMartiens_0[j - 1]->isAlive = false;
		nbMartien--;
		score += 300;
		// 2. retirer le laser du vaisseau
		unVaisseau.lesLasers[i] = unVaisseau.lesLasers[unVaisseau.nbLaser - 1];
		UIKit::gotoXY(laserVaisseau_x, laserVaisseau_y);
		cout << "  ";
		unVaisseau.nbLaser--;
	}
}
//////////////////////           REFAIRE METHODE (FIN)                //////////////////////////////////////////













bool Jeu::getGameOver_PlusDeMartien() {
	return gameOver_PlusDeMartien;
}

bool Jeu::getGameOver_VaisseauDetruit() {
	return gameOver_VaisseauDetruit;
}

void Jeu::afficher_gameOver() {
	for (int i = 0; i <= 5; i++)
		for (int j = 0; j <= 90; j++) {
		UIKit::gotoXY(40 + j, 20 + i);
		cout << " ";
		}
	if (gameOver_VaisseauDetruit == true){
		UIKit::cadre(42, 20, 179, 25, 5);
		UIKit::gotoXY(110, 22);
		cout << "VOUS ETES MORTS";
		UIKit::gotoXY(105, 23);
		cout << "Les martiens ont eu raison de vous !";
		UIKit::gotoXY(105, 28);
	}
	else if (gameOver_PlusDeMartien == true){
		UIKit::cadre(42, 20, 179, 25, 5);
		UIKit::gotoXY(110, 22);
		cout << "VOUS AVEZ GAGNER";
		UIKit::gotoXY(105, 23);
		cout << "Vous avez sauver la planete terre !";
		UIKit::gotoXY(105, 28);
	}
}
void Jeu::afficherInformations(LesMartiens &lesmartiens){
	UIKit::cadre(0, 0, 38, 57, 13);
	UIKit::gotoXY(10, 5);
	cout << "SCORE";
	UIKit::gotoXY(13, 6);
	cout << score;
	UIKit::color(2);
	UIKit::gotoXY(2, 15);
	cout << "Nombre de martien vert restant : ";
	UIKit::gotoXY(2, 16);
	cout << lesmartiens.nbMartien_0 << " ";
	UIKit::color(4);
	UIKit::gotoXY(2, 20);
	cout << "Nombre de martien rouge restant : ";
	UIKit::gotoXY(2, 21);
	cout << lesmartiens.nbMartien_1 << " ";
}

void Jeu::sauvegarder_score() {

	ofstream fichierEcriture("score.txt", ios::app);  // on ouvre en lecture
	if (fichierEcriture){

		fichierEcriture << score << endl;
		fichierEcriture.close();
	}
	else
		cout << "Impossible d'ouvrir le fichier !" << endl;
}