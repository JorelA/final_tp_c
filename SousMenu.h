#pragma once
#include <string>
#include <iostream>
#include <conio.h>
#include "Menu.h"
using namespace std;

#define SOUSMENU_DIFFICULTE 1

class SousMenu : public Menu {
private:
	string *sousMenu_options;
	int sousMenu_maxOptions;
	int sousMenu_nbOptions;
	int sousMenu_selectedOption;

public:

	//CONSTRUCTEUR
	SousMenu();

	//1.5 - MODIFICATIONS DU SOUS-MENU
	void sousMenu_addOption(string newOption);
	void sousMenu_clearOptions();
	void sousMenu_afficherCouleur(int option_menu, int indice, int posX, int posY);

	//4- SOUS MENU
	int navigationSousMenu(int optionChoisie);

	//ACCESSEUR SOUS-MENU
	int getSousMenu_nbOptions();
	int getSousMenu_selectedOption();

	//DESTRUCTEUR
	~SousMenu();
};