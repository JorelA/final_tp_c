#pragma once
#include <string>
#include <iostream>
#include <conio.h>
using namespace std;

#define FLECHE_HAUT 72
#define FLECHE_BAS 80
#define FLECHE_GAUCHE 75
#define FLECHE_DROITE 77
#define ENTER 13

#define QUATRIEME_OPTION 3
#define CINQUIEME_OPTION 4

class Menu {
private:
	string *options;
	int maxOptions;
	int nbOptions;
	int selectedOption;
public:
	int aggro;
	int vitesse;
	int nombre;
	string nom;
	//CONSTRUCTEUR NON PARAM�TR�
	Menu();

	//CONSTRUCTEUR PAR RECOPIE
	Menu(Menu &Menu);

	//METHODES
	//1- MODIFICATIONS DU MENU
	void addOption(string newOption);
	void removeOption(int idOption);
	void clearOption();

	//2- DESIGN VISUEL
	void loadingScreen();
	void afficherItem(string fichier_txt_sans_extension, int posX, int posY);
	void afficherSpaceInvaders();

	//3- NAVIGATION
	int navigationMenu();
	void afficherCouleur(int indice, int posX, int posY);

	//5- UTILITAIRES
	void attendre(int ms);
	void effacerZone(int nb_colonnes_a_effacer, int nb_ligne_a_effacer, int posX, int posY);

	//ACCESSEURS MENU
	int getMaxOption();
	int getNbOptions();
	string getOptions(int indice);
	int getSelectedOption();
	int getNombre();
	int getVitesse();
	int getAggro();
	string getNom();

	//SETTERS MENU
	void setAggro(int aggro);
	void setVitesse(int vitesse);
	void setNombre(int nombre);
	void setNom(string nom);


	//DESTROYER �_�
	~Menu();

};

