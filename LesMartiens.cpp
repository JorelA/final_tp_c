#include "LesMartiens.h"
#include "MonMartien.h"
#include "UIkit.h"
#include <conio.h>
#include <fstream>
#include <time.h>
#include "MethodesCommunes.h"

// ATTENTION: vitesse de laser doit etre plus rapide que mouvement du martien
#define DALAY_LASER_TYPE_0 100
#define DALAY_LASER_TYPE_1 20

LesMartiens::LesMartiens() {}

LesMartiens::LesMartiens(int delayMouvement, int delayTirer, int maxNbMartien){
	debutNouveauMartien = clock();
	debutTirer = clock();
	debutLaserType0 = clock();
	debutLaserType1 = clock();

	// ATTENTION: vitesse de laser doit etre plus rapide que mouvement du martien
	//delayLaserType0 = 100;
	//delayLaserType1 = 30;

	this->delayMouvement = delayMouvement;

	// delayNouveauMartien est plus grand que delayMouvement pour ne pas se toucher
	this->delayNouveauMartien = 5 * delayMouvement;
	estPremiereFois = true;

	this->delayTirer = delayTirer;

	//tableau des adresses des martiens
	nbMartien_0 = 10; // premiere fois, il y a 10 (type 0)
	tbMonMartiens_0 = new MonMartien*[nbMartien_0];

	//initialisatin des martiens de premiere partie
	for (int i = 0; i < 10; i++){
		tbMonMartiens_0[i] = new MonMartien(0, 1, delayMouvement); //type:0,  valeur:1 point
		tbMonMartiens_0[i]->isAlive = true;
		if (i == 0)
			tbMonMartiens_0[i]->coord.setPositionX(50);
		else  // espace entre les 2 martiens: 8
			tbMonMartiens_0[i]->coord.setPositionX(i * 16 + 30);
		tbMonMartiens_0[i]->coord.setPositionY(debutPosY);
	}

	//tableau des martiens de deuxieme partie
	nbMartien_1 = 0;
	this->maxNbMartien = maxNbMartien;
	nbMartienCache = this->maxNbMartien - 10;
	tbMonMartiens_1 = new MonMartien*[nbMartien_1];


	//initialisation des tableaux des lasers
	maxLasersType0 = 100;
	maxLasersType1 = 100;
	tbLasersType0 = new MonLaser[maxLasersType0];
	tbLasersType1 = new MonLaser[maxLasersType1];
	nbLaserType0 = 0;
	nbLaserType1 = 0;
}

//destructeur
LesMartiens::~LesMartiens(){
	//detruire le tableau des martiens
	for (int i = 0; i < nbMartien_0; i++)
		delete tbMonMartiens_0[i];
	delete[] tbMonMartiens_0;

	for (int i = 0; i < nbMartien_1; i++)
		delete tbMonMartiens_1[i];
	//delete[] tbMonMartiens_1;

	//detruire les tableaux des lasers
	delete[] tbLasersType0;
	delete[] tbLasersType1;
}


void LesMartiens::jiggleLesMartiens(){
	for (int i = 0; i < nbMartien_0; i++){
		tbMonMartiens_0[i]->afficher();
	}

	for (int i = 0; i < nbMartien_1; i++){
		tbMonMartiens_1[i]->afficher();
	}
}

void LesMartiens::creerNouveauxMartiens(){
	//les deuxiemes type de martien apparaitent apres de depart du jeu
	if (nbMartienCache > 0 && estPremiereFois){
		//premiere fois, 7 fois apres le mouvement des martiens de type 0
		if (clock() - debutNouveauMartien >= 2 * delayNouveauMartien){
			//renouveler debutNouveauMartien
			debutNouveauMartien = clock();

			srand((unsigned int)time(NULL));

			// type 1, valeur = 2
			tbMonMartiens_1[nbMartien_1] = new MonMartien(1, 2, delayMouvement);
			tbMonMartiens_1[nbMartien_1]->isAlive = true;

			//position de X aleatoire
			tbMonMartiens_1[nbMartien_1]->coord.setPositionX(rand() % 153 + 45);
			//position de Y fixe
			tbMonMartiens_1[nbMartien_1]->coord.setPositionY(debutPosY);

			nbMartien_1++;
			nbMartienCache--;
			//bloquer le delay de 4000 pour les prochaines fois
			estPremiereFois = false;
		}
	}
	else{
		if (nbMartienCache > 0 && clock() - debutNouveauMartien >= delayNouveauMartien){
			//renouveller debutNouveauMartien
			debutNouveauMartien = clock();

			srand((unsigned int)time(NULL));

			// type 1, valeur = 2
			tbMonMartiens_1[nbMartien_1] = new MonMartien(1, 2, delayMouvement);
			tbMonMartiens_1[nbMartien_1]->isAlive = true;

			//position de X aleatoire
			tbMonMartiens_1[nbMartien_1]->coord.setPositionX(rand() % 153 + 4);
			//position de Y fixe
			tbMonMartiens_1[nbMartien_1]->coord.setPositionY(debutPosY);

			nbMartien_1++;
			nbMartienCache--;
		}
	}
}

//pour un seule martien
void LesMartiens::ajouterUnLaser(int posX, int posY, int type){
	if (type == 0){
		//tableau dynamique
		if (nbLaserType0 == maxLasersType0){
			maxLasersType0 += 50;
			MonLaser* temp = new MonLaser[maxLasersType0];
			for (int i = 0; i < nbLaserType0; i++){
				temp[i] = tbLasersType0[i - 1];
			}
			delete[] tbLasersType0;
			tbLasersType0 = temp;
		}

		//initialiser et ajouter un nouveau laser
		MonLaser unLaser;
		// position de laser est deifferent de martien
		unLaser.init(posX + 1, posY + 1, type);
		//ajouter
		tbLasersType0[nbLaserType0] = unLaser;

		//aficher une fois a la position initiale
		UIKit::gotoXY(posX + 1, posY + 1);
		UIKit::color(VERT); // defini dans MonMartien.h
		cout << "<>";

		nbLaserType0++;
	}

	else{
		if (nbLaserType1 == maxLasersType1){
			//tableau dynamique
			maxLasersType1 += 50;
			MonLaser* temp = new MonLaser[maxLasersType1];
			for (int i = 0; i < nbLaserType1; i++){
				temp[i] = tbLasersType1[i - 1];
			}
			delete[] tbLasersType1;
			tbLasersType1 = temp;
		}

		//initialiser et ajouter un nouveau laser
		MonLaser unLaser;
		// position de laser est deifferent de martien
		unLaser.init(posX + 1, posY + 1, type);
		//ajouter
		tbLasersType1[nbLaserType1] = unLaser;

		//aficher une fois a la position initiale
		UIKit::gotoXY(posX + 1, posY + 1);
		UIKit::color(ROUGE); // defini dans MonMartien.h
		cout << "<>";

		nbLaserType1++;
	}
}

// pour simplifier, il n'y a pas de tableau dynamique dont la taille diminue automatiquement
void LesMartiens::superviser_retirer_Laser(){
	// parcourir tableau de lasers type 0
	for (int i = 0; i < nbLaserType0; i++) {
		int posX = tbLasersType0[i].getPosX();
		int posY = tbLasersType0[i].getPosY();
		// si atteindre la ligne plus bas
		if (posY == 50){
			//enlever sa place dans tableau
			tbLasersType0[i] = tbLasersType0[nbLaserType0 - 1];
			//effacer la derniere position
			UIKit::gotoXY(posX, posY);
			cout << "  ";

			nbLaserType0--;
		}
	}
	// parcourir tableau de lasers type 1
	for (int i = 0; i < nbLaserType1; i++) {
		int posX = tbLasersType1[i].getPosX();
		int posY = tbLasersType1[i].getPosY();
		// si atteindre la ligne plus bas
		if (posY == 50){
			//enlever sa place dans tableau
			tbLasersType1[i] = tbLasersType1[nbLaserType1 - 1];
			//effacer la derniere position
			UIKit::gotoXY(posX, posY);
			cout << "  ";

			nbLaserType1--;
		}
	}
}

//manipuler le tableau des martiens
void LesMartiens::tirer(){
	if (clock() - debutTirer >= delayTirer){
		debutTirer = clock();

		// si martien de type 0 est plus de 4, generer aleatoirement 4 pour tirer
		if (nbMartien_0 > 4){
			//generer aleatoirement 4 chiffres differents 
			srand((unsigned int)time(NULL));
			int tab[4];
			for (int i = 0; i < 4; i++){
				bool existe = true;
				do{
					int temp = rand() % nbMartien_0;
					tab[i] = temp;
					//si le meme nombre est deja genere
					int cpt = 0;
					while (cpt < i && tab[cpt] != temp)
						cpt++;
					existe = cpt == i ? false : true;
				} while (existe);
			}

			//faire tirer les 4 martiens genere
			for (int i = 0; i < 4; i++){
				int	posX = tbMonMartiens_0[tab[i]]->coord.getPositionX();
				int	posY = tbMonMartiens_0[tab[i]]->coord.getPositionY();
				int type = tbMonMartiens_0[tab[i]]->getType();

				ajouterUnLaser(posX, posY, type);
			}
		}
		else{  	//faire tirer tous les martiens si nbMartien est moins de 4
			for (int i = 0; i < nbMartien_0; i++){
				int	posX = tbMonMartiens_0[i]->coord.getPositionX();
				int	posY = tbMonMartiens_0[i]->coord.getPositionY();
				//	int type = tbMonMartiens_0[i]->getType();
				ajouterUnLaser(posX, posY, 0);
			}
		}

		// faire tirer tous les martiens de type 1
		for (int i = 0; i < nbMartien_1; i++){
			int	posX = tbMonMartiens_1[i]->coord.getPositionX();
			int	posY = tbMonMartiens_1[i]->coord.getPositionY();
			//	int type = tbMonMartiens_0[i]->getType();
			ajouterUnLaser(posX, posY, 1);
		}
	}
}

//manipuler les 2 tableaux des lasers
void LesMartiens::deplacerLaser(){
	if (clock() - debutLaserType0 >= DALAY_LASER_TYPE_0){
		for (int i = 0; i < nbLaserType0; i++)
			tbLasersType0[i].deplacer();
		debutLaserType0 = clock();
	}

	if (clock() - debutLaserType1 >= DALAY_LASER_TYPE_1){
		for (int i = 0; i < nbLaserType1; i++)
			tbLasersType1[i].deplacer();
		debutLaserType1 = clock();
	}

	/*
	 // TESTE
	 UIKit::gotoXY(1, 1);
	 cout << nbLaserType0 << endl;
	 cout << nbLaserType1 << endl;
	 cout << nbMartien << endl;
	 */
}