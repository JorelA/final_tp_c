#include "MonLaser.h"
#include "UIkit.h"
#include <iostream>
using namespace std;
#include "MonMartien.h"
#include <time.h>

MonLaser::MonLaser(){
}

void MonLaser::init(int posX, int posY, int type){
	coord.setPositionX(posX);
	coord.setPositionY(posY);
	this->type = type;
}

void MonLaser::init(int posX, int posY){
	coord.setPositionX(posX);
	coord.setPositionY(posY);
}

MonLaser::~MonLaser(){
}

int MonLaser::getPosX(){
	return coord.getPositionX();
}

int MonLaser::getPosY(){
	return coord.getPositionY();
}

void MonLaser::deplacer(){
	//effacer
	UIKit::gotoXY(getPosX(), getPosY());
	cout << "  ";

	//nouvelle position
	coord.setPositionY(coord.getPositionY() + 1);

	//afficher
	UIKit::gotoXY(getPosX(), getPosY());
	if (type == 0)
		UIKit::color(VERT); // defini dans MonMartien.h
	else
		UIKit::color(ROUGE); // defini dans MonMartien.h
	cout << "<>";
}

void MonLaser::deplacerLaserDeVaisseau(){
	//effacer
	UIKit::gotoXY(getPosX(), getPosY());
	cout << "  ";

	//nouvelle position
	coord.setPositionY(coord.getPositionY() - 1);

	//afficher
	UIKit::gotoXY(getPosX(), getPosY());
	UIKit::color(FOREGROUND_GREEN + FOREGROUND_RED + FOREGROUND_INTENSITY);
	cout << "^^";
}

