#pragma once
#include "Martien.h"
#include "MonLaser.h"

#define LARGEUR_Martien 4
#define HAUTEUR_Martien 2

#define VERT FOREGROUND_GREEN + FOREGROUND_INTENSITY
#define ROUGE FOREGROUND_RED + FOREGROUND_INTENSITY
#define BLEU FOREGROUND_BLUE + FOREGROUND_INTENSITY
#define BLANC FOREGROUND_GREEN + FOREGROUND_BLUE+FOREGROUND_RED+FOREGROUND_INTENSITY

class MonMartien : public Martien
{
private:
	int debutMouvement;
	int delayMouvement;
	bool horizontal;

	int delayBoucheOuerte;
	int debutBoucheOuerte;
	bool boucheOuverte;

public:
	MonMartien(int type, int valeur, int delayMouvement);
	~MonMartien();

	void removeExtraTerrestre() const;

	//etinceller d'un martien
	void afficher();
	void PutExtraTerrestre();

	void jiggleMartien();

	void afficherOuvert();
	void afficherFerme();

	int getType();

};


