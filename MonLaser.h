#pragma once
#include "Laser.h"
#include "Coord.h"

class MonLaser :public Laser
{
private:
	int type;

public:
	MonLaser();

	~MonLaser();
	
	void init(int posX, int posY, int type);
	void init(int posX, int posY);

	int getPosX();
	int getPosY();

	void deplacer();

	void deplacerLaserDeVaisseau();
};

