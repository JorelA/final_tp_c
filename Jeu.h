#pragma once
#include "LesMartiens.h"
#include "MonVaisseau.h"
#include "Menu.h"

class Jeu
{
private:
	// ATTENTION: maxNbMartien: a partir de 10
	// ATTENTION: delayMouvement: a partir de 250 (soit la vitesse du mouvement de bouche) (MonMartien.cpp)
	// PARAMETERES: (delayMouvement,delayTirer,maxNbMartien);
	LesMartiens lesmartiens;
	MonVaisseau monVaisseau;
	Menu leMenu;
	bool gameOver_PlusDeMartien;
	bool gameOver_VaisseauDetruit;
	int score = 0;
	string nom;
public:
	Jeu();
	~Jeu();

	void afficher_menu(Menu &leMenu);
	void former_martiens(LesMartiens &lesmartiens);
	void former_vaisseau_et_tirer_laser(MonVaisseau &unVaisseau);
	void afficherInformations(LesMartiens &lesmartiens);
	void afficher_gameOver();
	void sauvegarder_score();

	void superviserLaserMartien(LesMartiens & lesmartiens, MonVaisseau &unVaisseau);
	void superviserLaserVaisseau(LesMartiens & lesmartiens, MonVaisseau &unVaisseau);

	void _superviserLaserMartien(MonLaser* tbLasers, int nbLaser, MonVaisseau &unVaisseau);
	void testerCollisionMartien(MonMartien** tbMonMartiens, int &nbMartien, MonVaisseau &unVaisseau,
		int &laserVaisseau_x, int &laserVaisseau_y, int &score, int &i);

	//GETTERS
	bool getGameOver_PlusDeMartien();
	bool getGameOver_VaisseauDetruit();
};

