#include <string>
#include <iostream>
#include <time.h>
#include <conio.h>
#include <fstream>
#include "UIkit.h"
using namespace std;

void afficherItem(string filename, int posX, int posY) {

	//On cr�e un string
	string ascii;

	//On cr�er un ifstream ( variable qui stocke le contenu d'un fichier )
	ifstream Reader;

	//On ouvre le fichier gr�ce a son nom pass� en param�tre. DOIT �TRE UN .TXT pr�sent dans le root du TP
	Reader.open(filename + ".txt");

	//On boucle tant que on a pas fini de lire le fichier ( tant que NOT Reader.EndOfFile() )
	while (!Reader.eof())
	{
		//On va aux poisitions pass�s en param�tre
		UIKit::gotoXY(posX, posY);

		//On met la ligne du fichier ouvert dans la variable ascii
		getline(Reader, ascii);

		//On affiche la variable
		cout << ascii << endl;

		//On saute une ligne pour afficher
		posY++;
	}

	//On se place au m�me poisition initial -24 afin d'effacer le BOM, bytes order markes, qui sont 3 caract�re g�nant pour l'affichage au
	//d�but du fichier
	UIKit::gotoXY(posX, posY - 24);
	cout << " " << " " << " ";

	//On ferme le fichier
	Reader.close();
}

void attendre(int milliseconde) {
	int debut = clock();

	//On reste dans la boucle dans qu'on a pas atteint le nombre de millisecondes souhait�es
	while (clock() < debut + milliseconde);
}

void compter(){
	UIKit::color(FOREGROUND_GREEN + FOREGROUND_RED + FOREGROUND_INTENSITY);
	attendre(500);
	afficherItem("3", 70, 10);
	attendre(1000);
	afficherItem("2", 70, 10);
	attendre(1000);
	afficherItem("1", 70, 10);
	attendre(1000);
	// effacer le chiffre
	for (int i = 10; i < 21; i++){
		UIKit::gotoXY(70, i);
		for (int j = 0; j < 20; j++)
			cout << " ";
	}
	afficherItem("go !", 70, 12);
	attendre(500);
	// effacer "go !"
	for (int i = 10; i < 21; i++){
		UIKit::gotoXY(70, i);
		for (int j = 0; j < 30; j++)
			cout << " ";
	}
	attendre(300);
}

