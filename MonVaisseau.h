#pragma once
#include "Vaisseau.h"
#include "MonLaser.h"

class MonVaisseau:public Vaisseau
{
public:
	MonLaser lesLasers[400];
	int nbLaser;

	int debutTirer;
	//int delayTirer;

	int debutLaser;
	//int delayLaser;

protected:
	void removeVaisseau() const;
	void putVaisseau() const;

public:
	Coord coord;

	MonVaisseau();
	~MonVaisseau();

	void actionsVaisseau();

	void tirer();

	void deplacerLasers();

};



