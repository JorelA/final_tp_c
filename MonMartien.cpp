#include "MonMartien.h"
#include <iostream>
#include <time.h>
#include <conio.h>
#include "UIkit.h"
using namespace std;


MonMartien::MonMartien(int type, int valeur, int delayMouvement) : Martien(type, valeur){
	debutMouvement = clock();
	this->delayMouvement = delayMouvement;
	horizontal = true;

	delayBoucheOuerte = 250;  //vitesse de la bouche
	debutBoucheOuerte = clock();
	boucheOuverte = true;
}

MonMartien::~MonMartien()
{
}

//Redefinition la methode de ExtraTerrestre
void MonMartien::removeExtraTerrestre() const{
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << "    ";
	coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
	cout << "    ";
}

void MonMartien::PutExtraTerrestre(){
	if (typeExtraTerrestre == 0){ // le type 0
		coord.gotoXY(coord.getPositionX(), coord.getPositionY());
		cout << "{@@}";
		coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
		if (clock() - debutBoucheOuerte >= delayBoucheOuerte) {//	delay du mouvement de bouche;
			cout << "/\"\"\\";
			debutBoucheOuerte = clock();
		}
		else
			cout << " \\/" << endl;
	}
}

//etinceller d'un martien
// mettre la fin du jeu
void MonMartien::afficher() {
	if (clock() - debutBoucheOuerte >= delayBoucheOuerte){ //delay du mouvement de bouche;

		removeExtraTerrestre(); // effacer le martien

		jiggleMartien(); // modifier la position X, Y

		//premiere ligne de martien
		coord.gotoXY(coord.getPositionX(), coord.getPositionY());
		if (typeExtraTerrestre == 0){  // type 0
			UIKit::color(VERT);
			cout << "{@@}";
		}
		else{   // type 1
			UIKit::color(ROUGE);
			cout << "/MM\\";
		}
		
		//deuxiem ligne de martien
		coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
		if (boucheOuverte){
			if (typeExtraTerrestre == 0){ // type 0
				UIKit::color(VERT);
				cout << "/\"\"\\";
			}
			else{  // type 1
				UIKit::color(ROUGE);
				cout << "|~~|" << endl;
			}
		}
		else{
			if (typeExtraTerrestre == 0){ // type 0
				UIKit::color(VERT);
				cout << " \\/";
			}
			else{  // type 1
				UIKit::color(ROUGE);
				cout << "\\~~/";
			}
		}
		debutBoucheOuerte = clock();
		boucheOuverte = !boucheOuverte;

	}
}

void MonMartien::afficherOuvert(){
	UIKit::color(VERT);
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << "{@@}";
	coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
	cout << "/\"\"\\";
}

// seulement changer la position
void MonMartien::jiggleMartien()
{

	//  attendre
	if (clock() - debutMouvement >= delayMouvement){
		//reset temps de depart
		debutMouvement = clock();

		int posX = coord.getPositionX();
		int posY = coord.getPositionY();
		if (horizontal){
			//valeur aleatoire pour determiner un deplacement vers gauche ou droite
			int gauche_droite = rand() % 2;  // deux possibilite ( 0 ou 1)
			//bordure (0 et 160) de gauche et de droite (detecter tous les 2 cases)
			bool gauchInterdit = posX - 4 <= 40 || UIKit::getCharXY(posX - 4, posY) != ' ';
			bool droiteInterdit = posX + 4 >= 169 || UIKit::getCharXY(posX + 4, posY) != ' ';


			if (gauchInterdit && !droiteInterdit)
				coord.setPositionX(posX + 4);
			else if (!gauchInterdit && droiteInterdit)
				coord.setPositionX(posX - 4);
			else if (gauche_droite == 0 && !gauchInterdit)
				coord.setPositionX(posX - 4);
			else if (gauche_droite == 1 && !droiteInterdit)
				coord.setPositionX(posX + 4);
		}
		else
			coord.setPositionY(posY + 2); //mouvement vertical

		//pour la prochaine fois
		horizontal = !horizontal;
	}
}

int MonMartien::getType(){
	return typeExtraTerrestre;
}