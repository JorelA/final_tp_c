﻿#include <iostream>
#include <string>
#include "Menu.h"
#include <conio.h>
#include "UIkit.h"
#include <time.h>
#include <fstream>
#include "MethodesCommunes.h"
#include "MonMartien.h"
#include "LesMartiens.h"
#include "MonVaisseau.h"
#include "Jeu.h"
#include "MonVaisseau.h"
using namespace std;

int main(){
	Menu leMenu;
	Jeu leJeu;
	leJeu.afficher_menu(leMenu);
	UIKit::curseurVisible(false);
	int nbMartien = leMenu.getNombre();
	int aggressivite = leMenu.getAggro();
	int vitesse = leMenu.getVitesse();
	string nom = leMenu.getNom();
	// ATTENTION: maxNbMartien: a partir de 10
	// ATTENTION: delayMouvement: a partir de 250 (soit la vitesse du mouvement de bouche) (MonMartien.cpp)
	// PARAMETERES: (delayMouvement,delayTirer,maxNbMartien);
	LesMartiens lesmartiens(vitesse, aggressivite, nbMartien);
	
	MonVaisseau unVaisseau;

	//cout << nom;

	do{
		leJeu.former_martiens(lesmartiens);
		leJeu.former_vaisseau_et_tirer_laser(unVaisseau);

		leJeu.superviserLaserMartien(lesmartiens, unVaisseau);
		leJeu.superviserLaserVaisseau(lesmartiens, unVaisseau);

		leJeu.afficherInformations(lesmartiens);

	} while (leJeu.getGameOver_PlusDeMartien() != true && leJeu.getGameOver_VaisseauDetruit() != true);

	leJeu.afficher_gameOver();
	leJeu.sauvegarder_score();
	return 0;
}