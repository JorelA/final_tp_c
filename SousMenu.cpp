#pragma once
#include "SousMenu.h"
#include "Menu.h"
#include <string>
#include <iostream>
#include <time.h>
#include <fstream>
#include "UIkit.h"
using namespace std;

#define COMMENCER 0
#define DIFFICULTE 1
#define SCORES 2
#define OPT_AVANCEE 3
#define EXIT 4
#define MAX_TABLEAU_NOM 50
#define SOLDAT 0
#define SERGENT 1
#define COLONEL 2
#define CAPORAL 3
#define AMIRAL 4


//CONSTRUCTEUR NON PARAM�TR�
SousMenu::SousMenu() {
	sousMenu_maxOptions = 1;
	sousMenu_options = new string[sousMenu_maxOptions];
	sousMenu_nbOptions = 0;
	sousMenu_selectedOption = 0;
	/*setNombre(10);
	setAggro(300);
	setVitesse(500);*/
}

//M�thode qui AJOUTE une option au tableau dynamique de sousMenu_options
void SousMenu::sousMenu_addOption(string newOpt) {
	if (sousMenu_nbOptions == sousMenu_maxOptions) {

		string *temp = new string[sousMenu_maxOptions * 2];

		for (int i = 0; i < sousMenu_nbOptions; i++)
			temp[i] = sousMenu_options[i];

		delete[] sousMenu_options;
		sousMenu_options = temp;
		sousMenu_maxOptions *= 2;
	}

	sousMenu_options[sousMenu_nbOptions] = newOpt;
	sousMenu_nbOptions++;
}

//M�thode qui SUPPRIME TOUTE les options du tableau dynamique de sousMenu_options
void SousMenu::sousMenu_clearOptions() {
	delete[]sousMenu_options;
	sousMenu_maxOptions = 1;
	sousMenu_options = new string[sousMenu_maxOptions];
	sousMenu_nbOptions = 0;
	sousMenu_selectedOption = 0;
}

//M�thode qui renvoi l'int d'un caract�re pr�ss� et qui s'occupe de modifier l'option choisie ( sousMenu_selectedOption ) selon les touche pr�ss�
int SousMenu::navigationSousMenu(int option) {



	char key2 = getSousMenu_nbOptions() == 0 ? NULL : _getch();


	if (option == COMMENCER) {
		key2 = FLECHE_GAUCHE;
	}

	if (option == DIFFICULTE) {

		if (getSousMenu_nbOptions() < 1) {
			UIKit::cadre(89, 23, 105, 35, 4);
			sousMenu_addOption("Soldat");
			sousMenu_addOption("Sergent");
			sousMenu_addOption("Colonel");
			sousMenu_addOption("Caporal");
			sousMenu_addOption("Amiral");
		}

		if (key2 == FLECHE_HAUT && sousMenu_selectedOption != 0) {
			sousMenu_selectedOption--;
			effacerZone(20, 13, 107, 25);
		}

		else if (key2 == FLECHE_BAS && sousMenu_selectedOption != sousMenu_nbOptions - 1) {
			sousMenu_selectedOption++;
			effacerZone(20, 13, 107, 25);
		}

		else if (key2 == ENTER) {
			switch (sousMenu_selectedOption) {
			case SOLDAT:
				UIKit::gotoXY(107, 25);
				setVitesse(2000);
				setNombre(1);
				setAggro(4000);
				break;
			case SERGENT:
				UIKit::gotoXY(107, 27);
				setVitesse(1500);
				setNombre(15);
				setAggro(3500);
				break;
			case COLONEL:
				UIKit::gotoXY(107, 29);
				setVitesse(1500);
				setNombre(15);
				setAggro(3000);
				break;
			case CAPORAL:
				UIKit::gotoXY(107, 31);
				setVitesse(1000);
				setNombre(25);
				setAggro(2500);
				break;
			case AMIRAL:
				UIKit::gotoXY(107, 33);
				setVitesse(500);
				setNombre(30);
				setAggro(2000);
				break;
			}
			cout << "CHOIX CONFIRME";
		}

		sousMenu_afficherCouleur(option, sousMenu_selectedOption, 91, 25);
	}

	else if (option == SCORES) {
			{
				if (getSousMenu_nbOptions() < 1) {
					UIKit::cadre(89, 23, 140, 27, 4);
					sousMenu_addOption("Entrez votre pseudo : ");
				}
				ifstream fichierLecture("score.txt", ios::in);  // on ouvre en lecture
				string tabScoreNom[MAX_TABLEAU_NOM];
				string nom ;
				if (fichierLecture){
					int i = 0;
					UIKit::gotoXY(90, 24);
					cout << "Veuillez entrer votre pseudo : ";
					cin >> nom;
					setNom(nom);
					string contenu; 
					//On fait une boucle qui recupere chaque ligne du fichier texte,
					//et l'affecte dans un tableau de nom ou de score. le tableau de nom contiendra les ligne paire et celui de resultat contiendra les score
					while (getline(fichierLecture, contenu)) {
						tabScoreNom[i] = contenu;
						i++;
					} 
					fichierLecture.close();
				}
				else
					cout << "Impossible d'ouvrir le fichier !" << endl;

				int i = 0;
				while (i < MAX_TABLEAU_NOM && tabScoreNom[i] != nom) {
					i++;
				}

				UIKit::gotoXY(90, 25);
				if (i == MAX_TABLEAU_NOM){
					cout << "Nouveau joueur ajoute !";
					ofstream fichierEcriture("score.txt", ios::app);  // on ouvre en lecture
					if (fichierEcriture){
						fichierEcriture << nom << endl;
						fichierEcriture.close();
					}
					else
						cout << "Impossible d'ouvrir le fichier !" << endl;
				}
				else {
					cout << nom << " : dernier score = " << tabScoreNom[i + 1];
				}
				attendre(1500);
			
				
			}
		return FLECHE_GAUCHE;
	}

	else if (option == OPT_AVANCEE) {
		key2 = FLECHE_GAUCHE;
		//OPTION TROP COMPLIQU� MOMENTANEMENT DISABLE

		/*		{
		if (getSousMenu_nbOptions() < 1) {
		UIKit::cadre(89, 23, 105, 31, 4);
		sousMenu_addOption("Vitesse");
		sousMenu_addOption("Nombre");
		sousMenu_addOption("Agressivite");
		}

		else if (key2 == FLECHE_HAUT && sousMenu_selectedOption != 0) {
		sousMenu_selectedOption--;
		}
		else if (key2 == FLECHE_BAS && sousMenu_selectedOption != sousMenu_nbOptions - 1) {
		sousMenu_selectedOption++;
		}

		sousMenu_afficherCouleur(sousMenu_selectedOption, 91, 25);

		}*/
	}

	else if (option == EXIT)
		exit(0);

	return key2;
}

//M�thode qui affiche la ligne numero optionCouleur pass� en param�tre en couleur
void SousMenu::sousMenu_afficherCouleur(int option_menu, int optionCouleur, int posX, int posY) {
	//x68y22
	//Positions a l'interieur du cadre
	int colonne_depart = posY;
	int ligne_depart = posX;

	//On change la couleur
	UIKit::color(0);

	int i = 0;

	UIKit::cadre(posX - 2, posY - 5, posX + 14, posY - 3, 2);
	UIKit::gotoXY(posX, posY - 4);
	switch (option_menu){
	case DIFFICULTE:
		cout << "  DIFFICULTE";
		break;
	}
	//cout << Menu::getOptions(Menu::getSelectedOption());

	//De la 1ere option, jusqu'avant celle de en couleur, on affiche
	for (i = 0; i < optionCouleur; i++) {
		UIKit::gotoXY(ligne_depart, colonne_depart);
		cout << sousMenu_options[i] << "  " << endl;

		//Chaque tour on saute 2 lignes
		colonne_depart += 2;
	}

	//On change de couleur
	UIKit::color(4);

	//On se place � la poisitien suivante
	UIKit::gotoXY(ligne_depart, colonne_depart);

	//On affiche l'options a l'indice optionCouleur pass� en param�tre
	cout << sousMenu_options[optionCouleur] << " > " << endl;

	//On saute 2 lignes
	colonne_depart += 2;

	//On change de couleur
	UIKit::color(2);

	//De l'option en couleur +1 jusqu'au d'options total, on affiche
	for (i = optionCouleur + 1; i < sousMenu_nbOptions; i++) {
		UIKit::gotoXY(ligne_depart, colonne_depart);
		cout << sousMenu_options[i] << "  " << endl;
		colonne_depart += 2;
	}
}

//ACCESSEUR DE SOUSMENU_NBOPTIONs
int SousMenu::getSousMenu_nbOptions() {
	return sousMenu_nbOptions;
}
//ACCESSEUR DE SOUSMENU_SELECTEDOPTION
int SousMenu::getSousMenu_selectedOption() {
	return sousMenu_selectedOption;
}


//DESTRUCTEUR
SousMenu::~SousMenu() {
	delete[]sousMenu_options;
}