#include "MonVaisseau.h"
#include <iostream>
using namespace std;

#include "MethodesCommunes.h"

#define DELAY_TIRER_VAISSEAU 100
#define DELAY_LASER_VAISSEAU 10

MonVaisseau::MonVaisseau(){
	coord.setPositionX(80);
	coord.setPositionY(48);
	nbLaser = 0;

	//effcer le vieu vaisseau
	coord.gotoXY(20, 40);
	cout << " ";


	debutTirer = clock();
	//delayTirer = 100;

	debutLaser = clock();
	//delayLaser = 10;
}


MonVaisseau::~MonVaisseau(){
}


void MonVaisseau::removeVaisseau() const{
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	cout << "    ";
	coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
	cout << "    ";
}

void MonVaisseau::putVaisseau() const
{
	coord.gotoXY(coord.getPositionX(), coord.getPositionY());
	UIKit::color(FOREGROUND_BLUE + FOREGROUND_RED + FOREGROUND_INTENSITY);
	cout << "_HH_";
	coord.gotoXY(coord.getPositionX(), coord.getPositionY() + 1);
	cout << "UUUU";
}

void MonVaisseau::actionsVaisseau() {
	if (_kbhit()) {
		char touche = _getch();

		if (touche == ' ' && clock() - debutTirer >= DELAY_TIRER_VAISSEAU){
			debutTirer = clock();

			tirer();
		}
		else if (touche == 'k' && this->coord.getPositionX() > 41){
			removeVaisseau();
			coord.setPositionX(coord.getPositionX() - 1);
		}
		else if (touche == 'l'&&this->coord.getPositionX() < 175){

			removeVaisseau();
			coord.setPositionX(coord.getPositionX() + 1);
		}
	}
	putVaisseau();
}



void MonVaisseau::tirer(){
	//initialiser et ajouter un nouveau laser
	MonLaser unLaser;
	// position de laser est deifferent de martien
	unLaser.init(coord.getPositionX() + 1, coord.getPositionY() - 1);
	//ajouter
	lesLasers[nbLaser] = unLaser;

	//aficher une fois a la position initiale
	UIKit::gotoXY(coord.getPositionX() + 1, coord.getPositionY() - 1);
	UIKit::color(FOREGROUND_BLUE + FOREGROUND_INTENSITY);
	cout << "^^";

	nbLaser++;
}


void MonVaisseau::deplacerLasers(){
	if (clock() - debutLaser >= DELAY_LASER_VAISSEAU){
		debutLaser = clock();

		for (int i = 0; i < nbLaser; i++)
			lesLasers[i].deplacerLaserDeVaisseau();
	}
}
