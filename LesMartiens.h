#pragma once
#include "MonMartien.h"
#include <time.h>

#define debutPosY 3  // les martiens sont generes dans la meme ligne

class LesMartiens{
public:
	//variables pour creation des nouveaux martiens
	int delayNouveauMartien;
	int debutNouveauMartien;
	bool estPremiereFois;

	//variables pour tirage des martiens
	int delayTirer;
	int debutTirer;

	//variables pour le deplacement des lasers
	//int delayLaserType0;
	int debutLaserType0;
	//int delayLaserType1;
	int debutLaserType1;

	//varaible pour le mouvement de martien
	int delayMouvement;

	//tableau des objets martiens
	MonMartien** tbMonMartiens_0;
	int nbMartien_0;
	MonMartien** tbMonMartiens_1;
	int nbMartien_1;

	int maxNbMartien; // nombre total des martiens de type 0 et type 1

	// nombre de martien qui n'apparais pas tout de suite
	int nbMartienCache;

	//tableau pour les 2 types d'objet lasers
	MonLaser* tbLasersType0;
	int maxLasersType0;
	int nbLaserType0;

	MonLaser* tbLasersType1;
	int maxLasersType1;
	int nbLaserType1;

public:
	LesMartiens();
	LesMartiens(int delayMouvement, int delayTirer, int maxNbMartien);

	//destructeur
	~LesMartiens();

	void afficherDesMartiens();

	void affichageDebut();

	void jiggleLesMartiens();

	void creerNouveauxMartiens();

	//pour un seule martien
	void ajouterUnLaser(int posX, int posY, int type);

	void superviser_retirer_Laser();
	
	void tirer();

	void deplacerLaser();
};